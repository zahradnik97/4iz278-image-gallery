﻿<?php
  //načteme připojení k databázi a inicializujeme session
  require_once '../../inc/user.php';


  if(!empty($_GET["id"])){
    $sqlHasAccess = 'SELECT * FROM SEM_ma_pristup 
    INNER JOIN SEM_Album ON SEM_ma_pristup.AlbumId=SEM_Album.AlbumId 
    WHERE SEM_ma_pristup.UserId=:user_id AND SEM_ma_pristup.AlbumId=:album_id AND SEM_ma_pristup.IsOwner = :is_owner';
    $query = $db->prepare($sqlHasAccess);
    $query->execute([
      ':album_id'=>$_GET["id"],
      ':user_id'=>$_SESSION["user_id"],
      ':is_owner'=>1
    ]);
    $album = $query->fetch(PDO::FETCH_ASSOC);
    if(empty($album)){
      header('Location: /dashboard.php');
    }
}

$pageTitle = "Administrace alba";

if(isset($_POST["submit"])) {
$album["Name"] = $_POST["albumName"];
$album["Description"] = $_POST["albumDescription"];

$errors = [];
if(empty(trim($_POST["albumName"]))){
    $errors["albumName"] = "Musíte zadat název alba.";
}
if(empty(trim($_POST["albumDescription"]))){
    $errors["albumDescription"] = "Musíte zadat popis alba.";
}
if(empty($errors)){

  if(isset($_POST["albumId"])){
    $sqlUpdateAlbum = 'UPDATE SEM_Album SET Name=:name,Description=:description WHERE AlbumId=:album_Id LIMIT 1;';
    $query = $db->prepare($sqlUpdateAlbum);
    $query->execute([
      ':name'=>$_POST["albumName"],
      ':description'=>$_POST["albumDescription"],
      ':album_Id'=>$_POST["albumId"]
    ]);
  }
   else{
    $sqlInsertAlbum = 'INSERT INTO SEM_Album (name, description) VALUES (:name, :description);';
    $query = $db->prepare($sqlInsertAlbum);
    $query->execute([
      ':name'=>$_POST["albumName"],
      ':description'=>$_POST["albumDescription"]
    ]);
    $newAlbumId = $db->lastInsertId();
    $sqlAccess = 'INSERT INTO SEM_ma_pristup (AlbumId, UserId, IsOwner) VALUES (:album_id, :user_id, :is_owner);';
    $query = $db->prepare($sqlAccess);
    $query->execute([
      ':album_id'=>$newAlbumId,
      ':user_id'=>$_SESSION["user_id"],
      ':is_owner'=>1
    ]);
   }
   header('Location: '.BASE_URL.'dashboard.php');
}
else{
     //header('Location: '.BASE_URL.'album/edit.php?album='.$_POST["albumId"].'&image='.$_POST["imageId"]);
}

}

  include '../../inc/header.php';
echo '<a class="btn btn-primary" href="'.BASE_URL.'dashboard.php"><i class="far fa-arrow-alt-circle-left"></i> Zpět na přehled alb</a>'

?>
<div class="form-container col-md-6">

  <form action="edit.php" method="post">
  <div class="form-group">
      <label for="albumName">Název:</label>
      <input required oninvalid="this.setCustomValidity('Název nesmí být prázdný')"
            onchange="this.setCustomValidity('')" 
            class="form-control <?php echo (!empty($errors['albumName'])?'is-invalid':''); ?>" 
            type="text" value="<?php echo empty($album)  ? '' : htmlspecialchars(@$album["Name"]) ?>" 
            name="albumName" id="albumName">
       <?php
        echo (!empty($errors['albumName'])?'<div class="invalid-feedback">'.$errors['albumName'].'</div>':'');
      ?>
      </div>
      <div class="form-group">
      <label for="albumDescription">Popis:</label>
      <textarea required oninvalid="this.setCustomValidity('Popis nesmí být prázdný')"
                onchange="this.setCustomValidity('')" rows="4" 
                class="form-control <?php echo (!empty($errors['albumDescription'])?'is-invalid':''); ?>" 
                type="text" name="albumDescription" id="albumDescription"><?php echo empty($album) ? '' : htmlspecialchars(@$album["Description"]) ?></textarea>
      <?php
        echo (!empty($errors['albumDescription'])?'<div class="invalid-feedback">'.$errors['albumDescription'].'</div>':'');
      ?>
      </div>
      <div class="row">

      <div class="col-md-6">
        <button class="btn btn-success" type="submit" name="submit"><i class='far fa-save'></i> <?php echo empty($album) ? 'Vytvořit' : 'Uložit' ?></button>
      </div>

<?php 
    if(!empty($album)){
      echo '<div class="col-md-6 text-right">';
        if(!empty($_GET["id"])){
         echo '<input type="text" hidden name="albumId" value="'.$_GET["id"].'" id="albumId">';
         echo '<a class="btn btn-success" href="'.BASE_URL.'album/access.php?id='.$_GET["id"].'"><i class="far fa-edit"></i> Upravit přístup</a>';
         echo '<a class="btn btn-danger" href="'.BASE_URL.'album/delete.php?id='.$_GET["id"].'"><i class="far fa-trash-alt"></i> Odstranit</a>';
        }
      echo '</div>';
    }
  ?>
      </div>
  </form>
</div>


<?php
  //vložíme do stránek patičku
  include '../../inc/footer.php';