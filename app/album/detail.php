<?php
  //načteme připojení k databázi a inicializujeme session
  require_once '../../inc/user.php';
$pageTitle = "Detail alba";
$images = [];
$isOwner;
if(!empty($_GET["id"])){
    $sqlHasAccess = 'SELECT * FROM SEM_ma_pristup WHERE SEM_ma_pristup.UserId=:user_id AND SEM_ma_pristup.AlbumId=:album_id;';
    $query = $db->prepare($sqlHasAccess);
    $query->execute([
      ':user_id'=>$_SESSION["user_id"],
      ':album_id'=>$_GET["id"]
    ]);
    if($isOwner = $query->fetch(PDO::FETCH_ASSOC)){
        $sqlImagesSelect = 'SELECT * FROM SEM_Image WHERE SEM_Image.AlbumId=:album_id ORDER BY PostDate DESC;';
        $query = $db->prepare($sqlImagesSelect);
        $query->execute([
          ':album_id'=>$_GET["id"]
        ]);
        $images = $query->fetchAll(PDO::FETCH_ASSOC);
    }
    else{
        header('Location: '.BASE_URL.'dashboard.php');
    }
}
else{
    header('Location: '.BASE_URL.'dashboard.php');
}

  include '../../inc/header.php';
  echo '<a class="btn btn-primary" href="'.BASE_URL.'dashboard.php"><i class="far fa-arrow-alt-circle-left"></i> Zpět na přehled alb</a>';

echo '<div class="text-white text-right py-4 px-2">';
    echo '<a class="btn btn-success" href="'.BASE_URL.'image/edit.php?album='.$_GET['id'].'"><i class="fas fa-plus-square"></i> Nahrát fotku</a>';
echo '</div>';

echo '<div class="row">';
if(count($images) == 0){
    echo '<div class="col-sm-12 text-center">';
        echo '<i>V tomhle albu zatím nejsou přidáné žádné obrázky/fotky.</i>';
    echo '</div>';
  }
    foreach ($images as $key => $value) {
        echo '<div class="col-md-4">';
            echo '<div class="image-item">';
                echo '<p class="text-right">'.$value["PostDate"].'</p>';
                echo '<hr>';
                echo '<label for="image-name">Název:</label>';
                echo '<h1 class="text-center" id="image-name">'.htmlspecialchars(@$value["Name"]).'</h1>';
                echo '<div class="text-center">';
                    echo '<img height="150" src="'.BASE_URL.'image/get.php?name='.$value["ImagePath"].'"/>';
                echo '</div>';
                echo '<hr>';
                echo '<div class="card-body">';
                    echo '<label for="image-description">Popis:</label>';
                    echo '<p class="text-center" id="image-description">'.htmlspecialchars(@$value["Description"]).'</p>';
                echo '</div>';
                echo '<div class="row">';
                    echo '<div class="col-md-6 text-center">';
                        echo '<a href="'.BASE_URL.'image/detail.php?id='.$value["ImageId"].'" class="btn btn-primary">Otevřít</a>';
                    echo '</div>';
                    echo '<div class="col-md-6 text-center">';
                    if($isOwner["IsOwner"] || $value["UserId"] == $_SESSION["user_id"]){
                        echo '<a href="'.BASE_URL.'image/edit.php?album='.$_GET['id'].'&image='.$value["ImageId"].'" class="btn btn-success"><i class="far fa-edit"></i> Upravit</a>';
                    }
                    echo '</div>';
                echo '</div>';
            echo '</div>';
        echo '</div>';
    }
    echo '</div>';

?>
<?php
  //vložíme do stránek patičku
  include '../../inc/footer.php';