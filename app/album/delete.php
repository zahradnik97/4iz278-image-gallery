<?php

require_once '../../inc/user.php';

$sqlHasAccess = 'SELECT * FROM SEM_ma_pristup
INNER JOIN SEM_Album ON SEM_ma_pristup.AlbumId=SEM_Album.AlbumId 
WHERE SEM_ma_pristup.UserId=:user_id AND SEM_ma_pristup.AlbumId=:album_id';
$query = $db->prepare($sqlHasAccess);
$query->execute([
  ':album_id'=>$_GET["id"],
  ':user_id'=>$_SESSION["user_id"],
]);
$album = $query->fetch(PDO::FETCH_ASSOC);
if(!empty($album) && $album["IsOwner"] == 1){
    $sqlSelectImages = 'SELECT ImagePath FROM SEM_Image WHERE AlbumId=:album_id';
$query = $db->prepare($sqlSelectImages);
$query->execute([
  ':album_id'=>$_GET["id"]
]);
$images = $query->fetchAll(PDO::FETCH_ASSOC);

    foreach ($images as $key => $value) {
            unlink(__DIR__.'/../uploaded-images/'.$value["ImagePath"]);
    }

   $sqlDelete = 'DELETE FROM SEM_Album WHERE AlbumId=:album_id';
    $query = $db->prepare($sqlDelete);
   $query->execute([
    ':album_id'=>$_GET["id"]
   ]);
}
header('Location: /dashboard.php');
?>