﻿<?php
  //načteme připojení k databázi a inicializujeme session
  require_once '../../inc/user.php';

  $users = [];
  $activeUsers=[];
  $nonActiveUsers =[];
  if(!empty($_GET["id"])){
    $sqlHasAccess = 'SELECT * FROM SEM_ma_pristup 
    INNER JOIN SEM_Album ON SEM_ma_pristup.AlbumId=SEM_Album.AlbumId 
    WHERE SEM_ma_pristup.UserId=:user_id AND SEM_ma_pristup.AlbumId=:album_id AND SEM_ma_pristup.IsOwner = :is_owner';
    $query = $db->prepare($sqlHasAccess);
    $query->execute([
      ':album_id'=>$_GET["id"],
      ':user_id'=>$_SESSION["user_id"],
      ':is_owner'=>1
    ]);
    $album = $query->fetch(PDO::FETCH_ASSOC);
    if(empty($album)){
      header('Location: /dashboard.php');
    }
     $pageTitle = "Editovat přístup k albu ".$album["Name"];

     $sqlSelectActiveUsers = "SELECT SEM_User.UserId,Name,Email FROM SEM_User 
     LEFT JOIN SEM_ma_pristup ON SEM_ma_pristup.UserId = SEM_User.UserId
     WHERE SEM_ma_pristup.AlbumId = :album_id";
     $query = $db->prepare($sqlSelectActiveUsers);
     $query->execute([
      ':album_id'=>$album["AlbumId"]
    ]);
    $activeUsers = $query->fetchAll(PDO::FETCH_ASSOC);

    if(!empty($_GET["name"])){
    $sqlSelectUsers = "SELECT DISTINCT SEM_User.UserId,Name,Email FROM SEM_User 
    LEFT JOIN SEM_ma_pristup ON SEM_ma_pristup.UserId = SEM_User.UserId
    WHERE Name LIKE :name OR Email LIKE :name AND SEM_ma_pristup.AlbumId != :album_id OR SEM_ma_pristup.AlbumId IS null";
    $query = $db->prepare($sqlSelectUsers);
    $query->execute([
      ':name'=>'%'.$_GET["name"].'%',
      ':album_id'=>$album["AlbumId"]
    ]);
    $users = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($users as $key => $value) {
          $consists = false;
          foreach ($activeUsers as $activeKey => $activeValue) {
          if($value["UserId"] == $activeValue["UserId"]){
            $consists= true;
          }
        }
        if(!$consists){
          array_push($nonActiveUsers,$value);
        }
      }
}
  }
else{
      header('Location: /dashboard.php');
}

if(isset($_POST["submit"])) {
   header('Location: '.BASE_URL.'album/access.php?id='.$_POST["albumId"].'&name='.$_POST["name"]);
}

  include '../../inc/header.php';
echo '<a class="btn btn-primary" href="'.BASE_URL.'dashboard.php"><i class="far fa-arrow-alt-circle-left"></i> Zpět na přehled alb</a>';
 if(count($activeUsers) == 1){

      echo '<div class="col-sm-12 text-center">';
        echo '<i>Kromě Vás momentálně nemá do alba nikdo jiný přístup.</i>';
      echo '</div>';
      echo '<hr>';

    }else{
    
  echo '<h1 class="text-center">Uživatelé s přístupem</h1>';
  echo '<hr>';
  echo '<div class="row">';
  foreach ($activeUsers as $key => $value) {
    if($value["UserId"] != $_SESSION["user_id"]){
    echo'<div class="col-md-4 text-center">';
    echo '<div class="user-item ">';
    echo '<h3>'.htmlspecialchars(@$value["Name"]).'</h3>';
    echo '<p>'.htmlspecialchars(@$value["Email"]).'</p>';
    echo '<br><a href="'.BASE_URL.'album/remove-access.php?id='.$value["UserId"].'&album='.$_GET["id"].'" class="btn btn-danger"><i class="fas fa-trash-alt"></i> Odebrat přístup</a>';
    echo '</div>';
    echo '</div>';
  }
}
echo '</div><hr>';
}
?>
<h1 class="text-center">Přidání přístupu</h1>
<div class="form-container col-md-6">

<form action="access.php" method="post">

  <div class="form-group">
      <label for="name">Vyhledat uživatele:</label>
        <input placeholder="Jméno, Email obsahuje..." class="form-control" type="text" value="<?php echo empty($_GET["name"]) ? '' : htmlspecialchars(@$_GET["name"]) ?>" name="name" id="name">
      </div>
  <input hidden type="text" value="<?php echo $_GET["id"] ?>" name="albumId" id="albumId">
  <button class="btn btn-primary" type="submit" value="Vyhledat" name="submit"><i class="fas fa-search"></i> Vyhledat</button>
  </form>
</div>
<br>
<?php 

  

    if(!empty($nonActiveUsers)){
      echo '<div class="row">';
         foreach ($nonActiveUsers as $key => $value) {
            echo'<div class="col-md-4 text-center">';
            echo '<div class="user-item">';
            echo '<h3>'.htmlspecialchars(@$value["Name"]).'</h3>';
            echo '<p>'.htmlspecialchars(@$value["Email"]).'</p>';
            echo '<br><a href="'.BASE_URL.'album/add-access.php?id='.$value["UserId"].'&album='.$_GET["id"].'" class="btn btn-success"><i class="fas fa-plus-square"></i> Udělit přístup</a>';
            echo '</div>';
            echo '</div>';
        }
      echo '</div>';
    }
    else{
     echo '<p class="text-center">Žádné záznamy uživatelů nenalazeny.</p>';
	}
  ?>


<?php
  //vložíme do stránek patičku
  include '../../inc/footer.php';