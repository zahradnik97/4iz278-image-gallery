﻿<?php
  //načteme připojení k databázi a inicializujeme session
  require_once '../../inc/user.php';

  if(!empty($_GET["id"])){
    $sqlHasAccess = 'SELECT * FROM SEM_ma_pristup 
    INNER JOIN SEM_Album ON SEM_ma_pristup.AlbumId=SEM_Album.AlbumId 
    WHERE SEM_ma_pristup.UserId=:user_id AND SEM_ma_pristup.AlbumId=:album_id AND SEM_ma_pristup.IsOwner=:is_owner;';
    $query = $db->prepare($sqlHasAccess);
    $query->execute([
      ':album_id'=>$_GET["album"],
      ':user_id'=>$_SESSION["user_id"],
      ':is_owner'=>1
    ]);
    
    if($query->fetch(PDO::FETCH_ASSOC)){
      $sqlAccess = 'INSERT INTO SEM_ma_pristup (AlbumId, UserId, IsOwner) VALUES (:album_id, :user_id, :is_owner);';
      $query = $db->prepare($sqlAccess);
      $query->execute([
        ':album_id'=>$_GET["album"],
        ':user_id'=>$_GET["id"],
        ':is_owner'=>0
      ]);
    }
    header('Location: '.BASE_URL.'album/access.php?id='.$_GET["album"]);
     
}
else{
      header('Location: /dashboard.php');
}

?>