<?php
  //načteme připojení k databázi a inicializujeme session
  require_once '../../inc/user.php';
  $pageTitle = "Přehled alb";
  //$_SESSION['user_id']=$user['UserId'];
  //SELECT *           
//FROM book_mast INNER JOIN category ON book_mast.cate_id=category.cate_id WHERE book_mast.pub_lang="English";
$sqlSelectAlbums = 'SELECT SEM_Album.AlbumId,SEM_Album.Name,SEM_Album.Description,SEM_ma_pristup.IsOwner FROM SEM_ma_pristup 
INNER JOIN SEM_Album ON SEM_ma_pristup.AlbumId=SEM_Album.AlbumId
INNER JOIN SEM_User ON SEM_ma_pristup.UserId=SEM_User.UserId
WHERE SEM_ma_pristup.UserId=:user_id;';

  $query = $db->prepare($sqlSelectAlbums);
  $query->execute([
    ':user_id'=>$_SESSION["user_id"]
  ]);
  $albums=$query->fetchAll(PDO::FETCH_ASSOC);

  include '../../inc/header.php';

    echo '<div class="text-white text-right py-4 px-2">';
        echo '<a class="btn btn-success" href="'.BASE_URL.'album/edit.php"><i class="fas fa-plus-square"></i> Vytvořit album</a>';
    echo '</div>';
    if(count($albums) == 0){

      echo '<div class="col-sm-12 text-center">';
        echo '<i>Zatím nemáte přistup do žádného alba.</i>';
      echo '</div>';

    }else{

      echo '<h1 class"text-center">Alba do kterých máte přístup</h1><hr>';
      echo '<div class="row">';
        foreach ($albums as $key => $value) {
          echo '<div class="col-md-4">';
              echo '<div class="album-item mb-4">';
                  echo '<label for="album-name">Název:</label>';
                  echo '<h1 id="album-name" class="text-center">'.htmlspecialchars(@$value["Name"]).'</h1>';
                  echo '<hr>';
                    echo '<div class="card-body">';
                        echo '<label for="album-description">Popis:</label>';
                        echo '<p id="album-description">'.htmlspecialchars(@$value["Description"]).'</p>';
                    echo '</div>';
                    echo '<div class="row">';
                        echo '<div class="col-md-6 text-center">';
                        echo '<a class="btn btn-primary" href="'.BASE_URL.'album/detail.php?id='.$value["AlbumId"].'">Otevřít album</a>';
                        echo '</div>';
                        if($value["IsOwner"]){
                          echo '<div class="col-md-6 text-center">';
                          echo '<a class="btn btn-success" href="'.BASE_URL.'album/edit.php?id='.$value["AlbumId"].'"><i class="far fa-edit"></i> Upravit album</a>';
                          echo '</div>';
                        }
                    echo '</div>';
                echo '</div>';
            echo '</div>';
        }
      echo '</div>';
    }

?>
<?php
  //vložíme do stránek patičku
  include '../../inc/footer.php';