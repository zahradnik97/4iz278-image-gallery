<?php

require_once '../../inc/user.php';

$sqlHasAccess = 'SELECT * FROM SEM_Comment WHERE CommentId=:comment_id AND UserId=:user_id;';
$query = $db->prepare($sqlHasAccess);
$query->execute([
  ':comment_id'=>$_GET["id"],
  ':user_id'=>$_SESSION["user_id"],
]);
$comment = $query->fetch(PDO::FETCH_ASSOC);
if(!empty($comment)){

    $sqlDelete = 'DELETE FROM SEM_Comment WHERE CommentId=:comment_id AND UserId=:user_id;';
   $query = $db->prepare($sqlDelete);
    $query->execute([
    ':comment_id'=>$_GET["id"],
    ':user_id'=>$_SESSION["user_id"],
    ]);
}
header('Location: '.BASE_URL.'image/detail.php?id='.$comment["ImageId"]);
?>