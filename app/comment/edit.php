﻿<?php
  //načteme připojení k databázi a inicializujeme session
  require_once '../../inc/user.php';
  $pageTitle="Upráva komentáře";

  if(!empty($_GET["id"])){
    $sqlHasAccess = 'SELECT * FROM SEM_Comment 
    WHERE CommentId=:comment_id AND UserId=:user_id';
    $query = $db->prepare($sqlHasAccess);
    $query->execute([
      ':comment_id'=>$_GET["id"],
      ':user_id'=>$_SESSION["user_id"],
    ]);
    $comment = $query->fetch(PDO::FETCH_ASSOC);
    if(empty($comment)){
      header('Location: /dashboard.php');
    }
    
}

if(isset($_POST["submit"])) {

$errors=[];
  
if(empty(trim($_POST["content"]))){
    $errors["content"]="Komentář nesmí být prazdný";
}
if(empty($errors)){

    $sqlUpdateComment = 'UPDATE SEM_Comment SET Content=:content WHERE CommentId=:comment_id LIMIT 1;';
    $query = $db->prepare($sqlUpdateComment);
    $query->execute([  
      ':content'=>$_POST["content"],
      ':comment_id'=>$_POST["commentId"]
    ]);
   header('Location: '.BASE_URL.'image/detail.php?id='.$_POST["imageId"]);
}else{
    $_GET["image"] = $_POST["imageId"];
    $_GET["id"] = $_POST["commentId"];
    $comment["Content"] = $_POST["content"];
    $comment["Id"] = $_POST["commentId"];
}

}

  include '../../inc/header.php';
echo '<a class="btn btn-primary" href="'.BASE_URL.'image/detail.php?id='.$_GET["image"].'"><i class="far fa-arrow-alt-circle-left"></i> Zpět na detail fotografie</a>'

?>
<div class="form-container col-md-6">
  <form action="edit.php" method="post">
  <div class="form-group">
    <label for="content">Komentář:</label>
  <textarea oninvalid="this.setCustomValidity('Nemůžete vložit prázdný komentář')"
  required class="form-control <?php echo (!empty($errors['content']) ? 'is-invalid':''); ?>" required name="content" id="content"><?php echo htmlspecialchars(@$comment["Content"]) ?></textarea>
       <?php
        echo (!empty($errors['content'])?'<div class="invalid-feedback">'.$errors['content'].'</div>':'');
      ?>
</div>
  <button class="btn btn-primary" type="submit" name="submit"><i class="far fa-edit"></i> Upravit</button>
  <input type="text" hidden name="commentId" value="<?php echo $_GET["id"] ?>" id="commentId">
  <input type="text" hidden name="imageId" value="<?php echo $_GET["image"] ?>" id="imageId">
</form>
</div>

<?php
  //vložíme do stránek patičku
  include '../../inc/footer.php';