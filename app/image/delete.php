<?php

require_once '../../inc/user.php';

$sqlHasAccess = 'SELECT * FROM SEM_Image WHERE ImageId=:image_id AND UserId=:user_id;';
$query = $db->prepare($sqlHasAccess);
$query->execute([
  ':image_id'=>$_GET["id"],
  ':user_id'=>$_SESSION["user_id"],
]);
$image = $query->fetch(PDO::FETCH_ASSOC);
if(!empty($image)){
    
    unlink(__DIR__.'/../uploaded-images/'.$image["ImagePath"]);

    $sqlDelete = 'DELETE FROM SEM_Image WHERE ImageId=:image_id AND UserId=:user_id;';
   $query = $db->prepare($sqlDelete);
    $query->execute([
    ':image_id'=>$_GET["id"],
    ':user_id'=>$_SESSION["user_id"],
    ]);
}
header('Location: '.BASE_URL.'album/detail.php?id='.$_GET["album"]);
?>