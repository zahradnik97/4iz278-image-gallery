<?php
  require_once '../../inc/user.php';
// open the file in a binary mode
 
if(!empty($_GET["name"]) && !empty($_SESSION["user_id"]) ){
    $sqlHasAccess = 'SELECT * FROM SEM_image
    INNER JOIN SEM_ma_pristup ON SEM_ma_pristup.AlbumId = SEM_image.AlbumId
    WHERE ImagePath = :image_name AND SEM_ma_pristup.UserId = :user_id;';
    $query = $db->prepare($sqlHasAccess);
    $query->execute([
      ':image_name'=>$_GET["name"], 
      ':user_id'=>$_SESSION["user_id"]
    ]);
    if($query->fetch(PDO::FETCH_ASSOC)){
       $name = __DIR__.'/../uploaded-images/'.$_GET["name"];
        $fp = fopen($name, 'rb');

        // send the right headers
        // - adjust Content-Type as needed (read last 4 chars of file name)
        // -- image/jpeg - jpg
        // -- image/png - png
        // -- etc.
        header("Content-Type: image/jpeg");
        header("Content-Length: " . filesize($name));

        // dump the picture and stop the script
        fpassthru($fp);
        fclose($fp);
        exit;
    }
    else{
     exit;
     
	}
    
}
else{
    exit;
}

?>