﻿<?php
  //načteme připojení k databázi a inicializujeme session
  require_once '../../inc/user.php';
 
$image;
$comments = [];

if(!empty($_GET["id"])){
    $sqlHasAccess = 'SELECT * FROM SEM_Image WHERE ImageId=:image_id';
    $query = $db->prepare($sqlHasAccess);
    $query->execute([
      ':image_id'=>$_GET["id"]
    ]);
    $image = $query->fetch(PDO::FETCH_ASSOC);
    if(empty($image)){
      header('Location: /dashboard.php');
    }

    $sqlSelectComments = 'SELECT CommentId,Content,PostDate,Name FROM SEM_Comment
INNER JOIN SEM_User ON SEM_User.UserId = SEM_Comment.UserId
WHERE ImageId=:image_id
ORDER BY PostDate DESC;';
    $query = $db->prepare($sqlSelectComments);
    $query->execute([
      ':image_id'=>$_GET["id"]
    ]);
    $comments = $query->fetchAll(PDO::FETCH_ASSOC);
}
else{
      header('Location: /dashboard.php');
}


if(isset($_POST["submit"])) {

    //komentare
    if(!empty($_POST["comment"])){
         $sqlInsertComment = 'INSERT INTO SEM_Comment (ImageId, UserId, Content) VALUES (:image_id, :user_id, :content);';
    $query = $db->prepare($sqlInsertComment);
    $query->execute([
      ':image_id'=>$_POST["imageId"],
      ':user_id'=>$_SESSION["user_id"],
      ':content'=>$_POST["comment"]
    ]);
    header("Location: ".BASE_URL."image/detail.php?id=".$_POST["imageId"]);
	}
    else{
     header("Location: ".BASE_URL."image/detail.php?id=".$_POST["imageId"].'&error=true');
	}

}




$pageTitle = "Detail fotky ".htmlspecialchars(@$image["Name"]);
include '../../inc/header.php';
echo '<a class="btn btn-primary" style="margin-bottom:10px;" href="'.BASE_URL.'album/detail.php?id='.$image["AlbumId"].'"><i class="far fa-arrow-alt-circle-left"></i> Zpět na detail alba</a>'

?>
<div class="form-container col-md-12 image-detail" style="background:#e4e4e4;">
<div class="row">
<div class="col-md-7 text-center">

<?php
  echo '<label for="image-name">Název:</label>';
  echo '<h3 id="image-name">'.htmlspecialchars(@$image["Name"]).'</h3>';
  echo '<img width="400" src="'.BASE_URL.'image/get.php?name='.$image["ImagePath"].'" /> <br>';
  echo '<label for="image-description">Popis:</label>';
  echo '<p id="image-description">'.htmlspecialchars(@$image["Description"]).'</p>';
  echo '<p> Nahráno: '.$image["PostDate"].'</p>';
  ?>
</div>
<div class="col-md-5">
<?php 
      //echo '<a class="btn btn-danger" href="'.BASE_URL.'image/delete.php?id='.$_GET["image"].'&album='.$_GET["album"].'">Odstranit</a>';
    echo '<h3>Komentáře</h3>';
    echo '<hr>';
    if(count($comments) == 0){
      echo '<i>Zatím k tomuto obrázku neexistují žádné komentáře</i>';
    }
    foreach ($comments as $key => $value) {
            echo '<div class="comment-item">';
            echo '<h5>'.$value["Name"].' '.$value["PostDate"].'</h5>';
            echo '<div class="row">';
            echo '<div class="col-sm-8">';
            echo '<div class="card-body">';
            echo '<p>'.htmlspecialchars(@$value["Content"]).'</p>';
            echo '</div>';
            echo '</div>';
            echo '<div class="col-sm-4">';
            echo '<a class="btn btn-success" href="'.BASE_URL.'comment/edit.php?id='.$value["CommentId"].'&image='.$image["ImageId"].'"><i class="far fa-edit"></i></a>';
            echo '<a class="btn btn-danger" href="'.BASE_URL.'comment/delete.php?id='.$value["CommentId"].'"><i class="far fa-trash-alt"></i></a> ';
            echo '</div>';
            echo '</div>';
            echo '</div>';
          }
    echo '<hr>';
        ?>
<form action="detail.php" method="post">

  <div class="form-group">
  <label for="comment">Nový komentář:</label>
<div class="row">
  <div class="col-sm-8">
  
  <textarea required oninvalid="this.setCustomValidity('Nemůžete přidat prázdný komentář')"
   onchange="this.setCustomValidity('')"
  class="form-control <?php echo (!empty($_GET["error"]) ? 'is-invalid':''); ?>" name="comment" ></textarea>
       <?php
        echo (!empty($_GET["error"])?'<div class="invalid-feedback">Nemůžete přidat prázdný komentář.</div>':'');
      ?>
  </div>
  
  <div class="col-sm-4">
  <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-comment"></i> Přidat</button>
</div>
</div>

  </div>
 


<input type="text" hidden value="<?php echo $image["AlbumId"] ?>" name="albumId">
<input type="text" hidden value="<?php echo $image["ImageId"] ?>" name="imageId">

</form> 
</div>
</div>


  

</div>


<?php
  //vložíme do stránek patičku
  include '../../inc/footer.php';