<?php
  //načteme připojení k databázi a inicializujeme session
  require_once '../../inc/user.php';
 
$image;

if(!empty($_GET["image"])){
    $sqlHasAccess = 'SELECT * FROM SEM_Image WHERE ImageId=:image_id AND UserId=:user_id;';
    $query = $db->prepare($sqlHasAccess);
    $query->execute([
      ':image_id'=>$_GET["image"],
      ':user_id'=>$_SESSION["user_id"],
    ]);
    $image = $query->fetch(PDO::FETCH_ASSOC);
    if(empty($image)){
      header('Location: /dashboard.php');
    }
}

$pageTitle="Administrace obrázku";


if(isset($_POST["submit"])) {

$errors = "";
if(empty(trim($_POST["imageName"]))){
    $errors.="&errorName=true";
}

if(empty(trim($_POST["imageDescription"]))){
    $errors.="&errorDescription=true";
}

if($_FILES["fileToUpload"]["size"] == 0 && empty($_POST["imageId"])){
    $errors .= "&errorUpload=true";
}

if($errors == ""){

  if(isset($_POST["imageId"])){
    
    $sqlInsertImage = 'UPDATE SEM_Image SET Name=:name,Description=:description WHERE ImageId=:imageId LIMIT 1;';
    $query = $db->prepare($sqlInsertImage);
    $query->execute([
      ':name'=>$_POST["imageName"],
      ':description'=>$_POST["imageDescription"],
      ':imageId'=>$_POST["imageId"]
    ]);
  }
else{

if($_FILES["fileToUpload"]["size"] != 0){

    $target_dir = "../uploaded-images/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
  $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
  if($check !== false) {
    $uploadOk = 1;
  } else {
    $uploadOk = 0;
  }
  
  // kontrola velikosti souboru
  if ($_FILES["fileToUpload"]["size"] > 500000) {
    $uploadOk = 0;
  }
  
  //kontrola formatu obrazku
  if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
  && $imageFileType != "gif" ) {
    $uploadOk = 0;
  }
  
  if ($uploadOk != 0) {
    $sqlInsertImage = 'INSERT INTO SEM_Image (name, description, userId, albumId) VALUES (:name, :description, :userId, :albumId);';
    $query = $db->prepare($sqlInsertImage);
    $query->execute([
      ':name'=>$_POST["imageName"],
      ':description'=>$_POST["imageDescription"],
      ':userId'=>$_SESSION["user_id"],
      ':albumId'=>$_POST["albumId"]
    ]);
    $imageId = $db->lastInsertId();
    
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 70; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    $imagePath = $randomString.$imageId.'.'.$imageFileType;

    $sqlInsertImagePath = 'UPDATE SEM_Image SET ImagePath=:imagePath WHERE ImageId=:imageId LIMIT 1;';
    $query = $db->prepare($sqlInsertImagePath);
    $query->execute([
      ':imagePath'=>$imagePath,
      ':imageId' =>$imageId
    ]);  

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_dir.$imagePath)) {
      echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
      echo "Sorry, there was an error uploading your file.";
    }
  }
  }
}

header("Location: ".BASE_URL."album/detail.php?id=".$_POST["albumId"]);
}
else{
    header('Location: '.BASE_URL.'image/edit.php?album='.$_POST["albumId"].'&image='.$_POST["imageId"].$errors);
}
}



include '../../inc/header.php';
echo '<a class="btn btn-primary" href="'.BASE_URL.'album/detail.php?id='.$_GET["album"].'"><i class="far fa-arrow-alt-circle-left"></i> Zpět na detail alba</a>'

?>
<div class="form-container col-md-6">
  <form action="edit.php" method="post" enctype="multipart/form-data">
  <?php 
    if(empty($image)){
      echo '<div class="col-sm-12 text-center">';
      echo '<input class="inputfile" type="file" name="fileToUpload" id="fileToUpload">';
      echo '<label class="btn btn-success" for="fileToUpload"><i class="fas fa-upload"></i> Vybrat obrázek/fotku</label>';
        echo (!empty($_GET['errorUpload']) ? '<div class="invalid">Musíte nahrát obrázek.</div>':'');
      echo '</div>';
    }
  ?>
    <div class="form-group">
  <label for="imageName">Název:</label>
  <input required oninvalid="this.setCustomValidity('Název nesmí být prázdný')"
  onchange="this.setCustomValidity('')"
  class="form-control <?php echo (!empty($_GET['errorName']) ? 'is-invalid':''); ?>" type="text" value="<?php echo empty($image) ? '' : htmlspecialchars(@$image["Name"]) ?>" name="imageName" id="imageName">
       <?php
        echo (!empty($_GET['errorName']) ? '<div class="invalid-feedback">Název nesmí být prázdný.</div>':'');
      ?>
  </div>
    <div class="form-group">
    <label for="imageDescription">Popis:</label>
  <textarea required  oninvalid="this.setCustomValidity('Popis nesmí být prázdný')"
   onchange="this.setCustomValidity('')"
  class="form-control <?php echo (!empty($_GET['errorDescription'])? 'is-invalid':''); ?>" name="imageDescription" id="imageDescription"><?php echo empty($image) ? '' : htmlspecialchars(@$image["Description"]) ?></textarea>
       <?php
        echo (!empty($_GET['errorDescription']) ? '<div class="invalid-feedback">Popis nesmí být prázdný.</div>':'');
      ?>
  </div>

  <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-upload"></i> <?php echo empty($image) ? 'Nahrát' : 'Uložit' ?></button>
  <input type="text" hidden value="<?php echo $_GET["album"] ?>" name="albumId">
<?php 
    if(!empty($image)){
      echo '<a class="btn btn-danger" href="'.BASE_URL.'image/delete.php?id='.$_GET["image"].'&album='.$_GET["album"].'"><i class="far fa-trash-alt"></i> Odstranit</a>';
      echo '<input type="text" hidden value="'.$_GET["image"].'" name="imageId">';
      
    }
  ?>
</form>
</div>

<?php
  //vložíme do stránek patičku
  include '../../inc/footer.php';