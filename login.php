<?php
  //načteme připojení k databázi a inicializujeme session
  require_once 'inc/user.php';

  if (!empty($_SESSION['user_id'])){
    //uživatel už je přihlášený, nemá smysl, aby se přihlašoval znovu
    header('Location: index.php');
    exit();
  }

  $errors=false;
  if (!empty($_POST)){
    #region zpracování formuláře
    $userQuery=$db->prepare('SELECT * FROM SEM_User WHERE email=:email LIMIT 1;');
    $userQuery->execute([
      ':email'=>trim($_POST['email'])
    ]);
    if ($user=$userQuery->fetch(PDO::FETCH_ASSOC)){

      if (password_verify($_POST['password'],$user['password'])){
        //heslo je platné => přihlásíme uživatele
        $_SESSION['user_id']=$user['UserId'];
        $_SESSION['user_name']=$user['Name'];

        //smažeme požadavky na obnovu hesla
        //$forgottenDeleteQuery=$db->prepare('DELETE FROM forgotten_passwords WHERE user_id=:user;');
        //$forgottenDeleteQuery->execute([':user'=>$user['user_id']]);

        header('Location: /dashboard.php');
        exit();
      }else{
        $errors=true;
      }

    }else{
      $errors=true;
    }
    #endregion zpracování formuláře
  }

  $pageTitle="Přihlášení";
  //vložíme do stránek hlavičku
?>

  <h2>Přihlášení uživatele</h2>
  <form method="post">
    <div class="form-group text-left">
      <label for="email">E-mail:</label>
      <input type="email" name="email" id="email" required class="form-control <?php echo ($errors?'is-invalid':''); ?>" value="<?php echo htmlspecialchars(@$_POST['email'])?>"/>
      <?php
        echo ($errors?'<div class="invalid-feedback">Neplatná kombinace přihlašovacího e-mailu a hesla.</div>':'');
      ?>
    </div>
    <div class="form-group text-left">
      <label for="password">Heslo:</label>
      <input type="password" name="password" id="password" required class="form-control <?php echo ($errors?'is-invalid':''); ?>" />
    </div>
    <button type="submit" class="btn btn-primary">Přihlásit se</button>
    <br>
    <a href="registration.php" class="btn btn-light">Registrovat se</a>
  </form>