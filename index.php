<?php
  //načteme připojení k databázi a inicializujeme session
  require_once 'inc/user.php';
  //načteme inicializaci knihovny pro Facebook
  require_once 'inc/facebook.php';

  $pageTitle= "Vítejte v aplikaci";
  //vložíme do stránek hlavičku
  include __DIR__.'/inc/header.php';

  if (!empty($_SESSION['user_id'])){
    header('Location: '.BASE_URL.'dashboard.php');
  }else{
    
    echo '<div class="form-container text-center col-md-6">';
    require_once 'login.php';

    #region přihlašování pomocí Facebooku
    //inicializujeme helper pro vytvoření odkazu
    $fbHelper = $fb->getRedirectLoginHelper();

    //nastavení parametrů pro vyžádání oprávnění a odkaz na přesměrování po přihlášení
    $permissions = ['email'];
    $callbackUrl = htmlspecialchars('http://localhost:8080/fb-callback.php');

    //necháme helper sestavit adresu pro odeslání požadavku na přihlášení
    $fbLoginUrl = $fbHelper->getLoginUrl($callbackUrl, $permissions);

    //vykreslíme odkaz na přihlášení
    echo ' <a href="'.$fbLoginUrl.'" class="btn btn-primary"><i class="fab fa-facebook-square"></i> Přihlásit se pomocí Facebooku</a>';
    #endregion přihlašování pomocí Facebooku

    echo '</div>';
  }

  //vložíme do stránek patičku
  include __DIR__.'/inc/footer.php';