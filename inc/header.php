<!DOCTYPE html>
<html lang="cs">
  <head>
    <title>Semestralni prace - zaht02</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" 
    integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo BASE_URL ?>resources/css/styles.css?<?php echo time() ?>">
  </head>
  <body>
    <header class="bg-dark">
    <div class="header-overlay">
      <h1 class="text-white text-center py-4 px-2"><?php echo $pageTitle ?></h1>
      <div class="text-white text-right py-4 px-2">
      <?php if (!empty($_SESSION['user_id'])){
    echo '<p>Přihlášený uživatel: <strong>'.htmlspecialchars($_SESSION['user_name']).'</strong></p>';
    echo '<a href="'.BASE_URL.'logout.php" class="btn btn-danger"><i class="fas fa-sign-out-alt"></i> Odhlásit se</a>';
    } ?>
      </div>
      </div>
    </header>
    <main class="container pt-2">